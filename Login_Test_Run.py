import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from Selectors import *


class LoginTest(unittest.TestCase):

    # Opening browser.
    def setUp(self):
        self.driver = webdriver.Chrome(r'C:\chromedriver.exe')

    # Testing Login
    def testLogin(self):
        pageUrl = "https://www-gli.singulardev.uk"
        driver = self.driver
        driver.maximize_window()
        driver.get(pageUrl)

        # Wait until page turns from server to desktop version
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, html_code)))

        # Sending Login Data
        driver.find_element_by_xpath(user_field).send_keys(uname)
        driver.find_element_by_xpath(pass_field).send_keys(password)
        driver.find_element_by_xpath(login_button).click()

        # Checking if user_id is availeble, also ID and Username assertions
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, user_id)))
        assert ID == driver.find_element_by_xpath(user_id).text
        assert uname == driver.find_element_by_id("userName").text

    # Closing the browser.
    def CloseBrowser(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
